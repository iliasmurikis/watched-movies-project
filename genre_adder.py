import psycopg2
from imdb import IMDb

try:
    conn = psycopg2.connect(
        host="localhost",
        database="Movies",
        user="postgres",
        port="5432"
    )
    cur = conn.cursor()
    print("Connection Established")
except Exception as error:
    print(error)

imdb = IMDb()

# Get all distinct movie_ids
query = "SELECT movie_id FROM watched_movies;"
cur.execute(query)
all_ids = cur.fetchall()

# For each id, fetch all the movie's genres
for id in all_ids:
    # Getting the object of the movie
    movie = imdb.get_movie(id[0][2:])
    # movie = imdb.get_movie('0076759')

    # Loop to iterate between the genres of each movie
    for genre in movie['genres']:
        query2 = "INSERT INTO watched_movies_genres(movie_id, genre) VALUES ('%s', '%s') ON CONFLICT DO NOTHING;" % \
                 (id[0], genre)

        try:
            cur.execute(query2)
            conn.commit()
            print(query2)
        except Exception as e:
            print(id, genre)
            print(e)

cur.close()
conn.close()
