import psycopg2
from imdb import IMDb

try:
    conn = psycopg2.connect(
        host="localhost",
        database="Movies",
        user="postgres",
        port="5432"
    )
    cur = conn.cursor()
    print("Connection Established")
except Exception as error:
    print(error)


imdb = IMDb()

movie_id = input("Enter the IMDB ID (tt0000000): ")
movie_item = imdb.get_movie(movieID=movie_id[2:])
movie_title = movie_item['title']
if "'" in movie_title:
    print("True 1")
    movie_title = movie_title.replace("'", "")
movie_release_year = imdb.get_movie_release_dates(movieID=movie_id[2:])['data']['raw release dates'][0]['date'][-4:]
movie_director = movie_item['directors'][0]['name']
if "'" in movie_director:
    movie_director = movie_director.replace("'", "")
    print("True 2")

query = "INSERT INTO watched_movies(movie_id, title, release_year, director) VALUES ('%s', '%s', %s, '%s') ON CONFLICT DO NOTHING" % \
        (movie_id, movie_title, movie_release_year, movie_director)
print(query)

try:
    cur.execute(query)
    conn.commit()
except Exception as e:
    print(movie_id)
    print(e)

cur.close()
conn.close()
