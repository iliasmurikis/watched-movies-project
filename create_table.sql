create table watched_movies
(
    movie_id     text not null
        constraint watched_movies_pk
            primary key,
    title        text,
    release_year smallint,
    director     text,
    duration     integer,
    cover        text,
    full_cover   text
);

create table watched_movies_genres
(
    movie_id text not null,
    genre    text not null,
    constraint watched_movies_genre_pk
        primary key (movie_id, genre),
    constraint fk_movie_id 
        foreign key (movie_id)
            references watched_movies
);


create table watched_movies_writers
(
    movie_id text not null,
    writer   text not null,
    constraint watched_movies_writers_pk
        primary key (movie_id, writer),
    constraint fk_movie_id 
        foreign key (movie_id)
            references watched_movies
);
