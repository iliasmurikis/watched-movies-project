# Watched Movies Project

## Introduction
This project began with the intention to gather a list of all watched movies from the Popcorn Time application and store them into a personal database. A second step is to gather information about the movies from [IMDb](https://www.imdb.com/).<br>
All the scripts are written in Python, plus one SQL script for the databases creation.

## Databases
For this project a database was created in PostgreSQL with three tables. The first table contains most of the information, which is movie ID, titile, realease year, director, duration, link for the cover and link for the full resolution cover. The second table contains the different genres that a movie is classified. Attention, a movie can have more than one genre. And the third table contains the writers of a movie. As in the previous table, in this one too, a movie can have more than one writer.<br>
Below you can see three samples from the database tables.
| movie_id | title | release_year | director | duration | cover | full_cover |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| tt5052448 | Get Out | 2017 | Jordan Peele | 104 | "https://m.media-amazon.com/images/M/MV5BMjUxMDQwNjcyNl5BMl5BanBnXkFtZTgwNzcwMzc0MTI@._V1_SY150_CR0,0,101,150_.jpg" | "https://m.media-amazon.com/images/M/MV5BMjUxMDQwNjcyNl5BMl5BanBnXkFtZTgwNzcwMzc0MTI@.jpg" |
| tt0356910 | Mr. & Mrs. Smith | 2005 | Doug Liman | 120 | "https://m.media-amazon.com/images/M/MV5BMTUxMzcxNzQzOF5BMl5BanBnXkFtZTcwMzQxNjUyMw@@._V1_SY150_CR0,0,101,150_.jpg" | "https://m.media-amazon.com/images/M/MV5BMTUxMzcxNzQzOF5BMl5BanBnXkFtZTcwMzQxNjUyMw@@.jpg" |
| tt1345836 | The Dark Knight Rises | 2012 | Christopher Nolan | 164 | "https://m.media-amazon.com/images/M/MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@._V1_SY150_CR0,0,101,150_.jpg" | "https://m.media-amazon.com/images/M/MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@.jpg" |
| tt0304141 | Harry Potter and the Prisoner of Azkaban | 2004 | Alfonso Cuarón | 142 | "https://m.media-amazon.com/images/M/MV5BMTY4NTIwODg0N15BMl5BanBnXkFtZTcwOTc0MjEzMw@@._V1_SY150_CR0,0,101,150_.jpg" | "https://m.media-amazon.com/images/M/MV5BMTY4NTIwODg0N15BMl5BanBnXkFtZTcwOTc0MjEzMw@@.jpg" |
| tt1950186 | Ford v Ferrari | 2019 | James Mangold | 152 | "https://m.media-amazon.com/images/M/MV5BM2UwMDVmMDItM2I2Yi00NGZmLTk4ZTUtY2JjNTQ3OGQ5ZjM2XkEyXkFqcGdeQXVyMTA1OTYzOTUx._V1_SY150_CR0,0,101,150_.jpg" | "https://m.media-amazon.com/images/M/MV5BM2UwMDVmMDItM2I2Yi00NGZmLTk4ZTUtY2JjNTQ3OGQ5ZjM2XkEyXkFqcGdeQXVyMTA1OTYzOTUx.jpg" |

|movie_id | genre |
| ------- | ----- |
| tt0373889 | Action |
| tt0373889 | Adventure |
| tt0373889 | Family |
| tt0373889 | Fantasy |
| tt0373889 | Mystery |

|movie_id | writer |
| ------- | ----- |
| tt0373889 | Michael Goldenberg |
| tt0373889 | J.K. Rowling |
| tt0112462 | Bob Kane |
| tt0112462 | Lee Batchler |
| tt0112462 | Janet Scott Batchler |

"movie_id" is a foreign key to all three tabels.

## Insert movies into the database from file
With the Python script [file_to_db.py](https://gitlab.com/iliasmurikis/watched-movies-project/-/blob/master/file_to_db.py) all the seen movies that are stored in the watched.db file that is generated from Popcorn Time, are read line by line and for each one, relevant information is fetched from the [IMDb API](https://developer.imdb.com/), processed and stored into the database. The management of the API is done throught the Python librady [IMDbPY](https://imdbpy.github.io/).

## For every new movie...
Since no one stops watching movies, this project is future proof. The user can add a new watched movie by simply executing the [user_input_to_db.py](url) file in the terminal:
<br>
`python3 /my_path/watched_movies_project/user_input_to_db.py`
<br>
and a cursor will indicate where to write the IMDb code of the watched movie.

## Plots for the project
In the [Notebook](https://gitlab.com/iliasmurikis/watched-movies-project/-/blob/master/Watched_Movies.ipynb) you can find all the plot that I have create using all the seen movies from my database. In there you can find all the database's information visualised.

## Additional code
In the first implementation of the project, the idea of the genre had not come up, so the [genre_adder.py](https://gitlab.com/iliasmurikis/watched-movies-project/-/blob/master/genre_adder.py) created to add a postreriori the different genres for the already stored movies in the database.
