from bs4 import BeautifulSoup
import requests
import pandas.io.sql as sqlio
import pandas as pd


def rating_for_movie(movie_id):
    url = 'https://www.imdb.com/title/' + movie_id
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    rating = soup.find_all('span', {'class': 'sc-7ab21ed2-1 jGRxWM'})[0].get_text()

    return rating


def retrieve_ratings_for_movies(movie_id_list):
    ratings = []
    for movie_id in movie_id_list:
        ratings.append(rating_for_movie(movie_id))

    d = {'movie_id': movie_id_list, 'rating': ratings}

    return pd.DataFrame(d)


def get_list_of_watched_movies(conn, director):
    query = "select movie_id from watched_movies where director = '" + director + "';"
    df = sqlio.read_sql_query(query, conn)

    return df['movie_id'].to_numpy()


def id_to_title(conn, movie_id):
    query = "select title from watched_movies where movie_id='" + movie_id + "';"
    df = sqlio.read_sql_query(query, conn)

    return df['title'].to_numpy()
